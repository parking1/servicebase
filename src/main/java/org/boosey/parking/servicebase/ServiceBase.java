package org.boosey.parking.servicebase;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import org.boosey.parking.events.EventBase;
import javax.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class ServiceBase{

    private Map<UUID, EventBase<? extends EventBase<?>>> subscriptions = new HashMap<>();

    protected <V extends EventBase<V>> void createSubscription(EventBase<V> e, Consumer<V> handler) {
        try {
            @SuppressWarnings("unchecked")
            UUID subscriptionUUID = e.subscribe(handler, (Class<V>) e.getClass());
            subscriptions.put(subscriptionUUID, e);
        } catch (Exception ex) {
            log.info("EXCEPTION: {}", ex);
        }
    }

    protected <V extends EventBase<V>> void destroyAllSubscriptions() {
        subscriptions.forEach((uuid, e) -> {     
            e.unsubscribe(uuid, (Class<EventBase<V>>) e.getClass());
        });
        subscriptions.clear();
    } 
}